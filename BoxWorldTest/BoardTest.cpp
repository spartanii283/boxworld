#include "pch.h"
#include "CppUnitTest.h"
#include "../BoxWorld/Board.h"
#include "../BoxWorld/Wall.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoxWorldTest
{
	TEST_CLASS(BoardTest)
	{
	public:

		TEST_METHOD(CheckBoxState)
		{
			Board board;
			board.SetLevelFormat(1);

			std::pair<uint16_t, uint16_t> wallPosition(3, 6);
			std::make_shared<Wall>(wallPosition);

			bool isWall = board.CheckBoxState(wallPosition);

			Assert::IsTrue(isWall);
		}

		TEST_METHOD(GetPlayerPositionInGame) 
		{
			Board board;
			board.SetLevelFormat(2);
			std::pair<uint16_t, uint16_t> playerPosition = board.GetPlayerPositionInGame();
			std::pair<uint16_t, uint16_t> expectedPlayerPosition(4, 4);
			
			Assert::IsTrue(playerPosition == expectedPlayerPosition);
		}

		TEST_METHOD(IsBallInList)
		{
			Board board;
			board.SetLevelFormat(1);
			Elements::Position expectedBallPositionInLevel(4, 7);

			bool isBall = board.IsBallInList(expectedBallPositionInLevel);

			Assert::IsTrue(isBall);
		}
	};
}