#include "pch.h"
#include "CppUnitTest.h"

#include "SFML/Graphics.hpp"
#include "../BoxWorld/Level.h";
#include "../BoxWorld/Board.h";

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoxWorldTest
{
	TEST_CLASS(LevelTest)
	{
	public:

		TEST_METHOD(SetFileName)
		{
			Board board;
			uint16_t level = 2;
			board.SetLevelFormat(level);
			std::string expectedFileName = "Levels/level2.txt";

			Level levelObj = board.GetLevelFormat();
			std::string result = levelObj.GetFileName();

			Assert::AreSame(expectedFileName, result);
		}

		TEST_METHOD(SetInitialPositions)
		{
			Board board;
			uint16_t level = 1;
			uint16_t expectedNumberOfBoxesAndBalls = 8;
			board.SetLevelFormat(level);

			Level playedLevel = board.GetLevelFormat();
			uint16_t numberOfBoxesAndBallsFromLevel = playedLevel.GetNumberOfBoxesAndBalls();

			Assert::IsTrue(expectedNumberOfBoxesAndBalls == numberOfBoxesAndBallsFromLevel);
		}
	};
}
