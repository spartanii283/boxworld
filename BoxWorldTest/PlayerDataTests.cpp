#include "pch.h"
#include "CppUnitTest.h"

#include "../BoxWorld/PlayerData.h";

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoxWorldTest
{
	TEST_CLASS(PlayerDataTest)
	{
	public:

		TEST_METHOD(Constructor)
		{
			std::array <PlayerData::BestScore, NUMBER_OF_LEVELS> bestScore;
			uint16_t lastLevel = 2;
			PlayerData playerData(bestScore, lastLevel);

			Assert::IsTrue(
				playerData.GetLastLevel() == 2
			);
		}

		TEST_METHOD(GetBestScoreOnLevel) {

			PlayerData playerData;
			std::pair<uint16_t, uint16_t> score = playerData.GetBestScoreOnLevel(1);
			std::pair<uint16_t, uint16_t> expectedBestScore(10, 6);

			Assert::IsTrue(score == expectedBestScore);
		}

		TEST_METHOD(SetBestScoreOnLevel) {

			uint16_t level = 1;
			PlayerData playerData;
			std::pair<uint16_t, uint16_t> score = playerData.GetBestScoreOnLevel(level); //10, 6
			std::pair<uint16_t, uint16_t> newScore(7, 5);
			playerData.SetBestScore(newScore, level);
			std::pair<uint16_t, uint16_t> expectedBestScore = playerData.GetBestScoreOnLevel(level);


			Assert::IsTrue(score != expectedBestScore);
		}

		TEST_METHOD(IsBestScore) {
			PlayerData playerData;
			uint16_t level = 2;
			uint16_t playerMoves = 10;

			playerData.SetPlayerMoves(playerMoves);
			bool isBestScore = playerData.IsBestScore(level);

			Assert::IsTrue(isBestScore);
		}
	};
}
