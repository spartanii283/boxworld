#include "pch.h"
#include "CppUnitTest.h"
#include "../BoxWorld/Player.h"
#include "../BoxWorld/Elements.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace BoxWorldTest
{
	TEST_CLASS(PlayerTest)
	{
	public:

		TEST_METHOD(Constructor)
		{
			auto position = Elements::Position(2, 3);
			Player player(position);

			Assert::IsTrue(position == player.GetPosition());
		}

	};
}
