#pragma once
#include "Elements.h"
#include "Constants.h"
#include <cstdint>
#include <array>

class Box : public Elements
{
public:
	Box();
	Box(Position position);
	Box(const Box& box2);
	Box& operator =(const Box& box2);

	//red-> box on ball, yellow->empty box
	//true=red, false=yellow
	void SetBoxColor(bool color);

	bool GetBoxColor() const;
	std::array<std::pair<uint8_t, uint8_t>, 4> GetAvailableBoxMoves() const;

	friend std::ostream& operator<<(std::ostream& out, const Box& box);
	~Box();

private:
	bool m_color;
	sf::Texture m_boxTexture;
	std::array<std::pair<uint8_t, uint8_t>, 4> m_availableBoxMoves;
};