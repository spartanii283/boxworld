#include "Ball.h"

Ball::Ball(Position position): Elements(position)
{
	m_show = true;

	if(!m_ballTexture.loadFromFile("../Resources/Ball.png"))
	{
		std::cout << "Error could not load ball image" << std::endl;
	}

	this->setTexture(m_ballTexture);
	this->setPosition(position.second * SPRITE_SIZE, position.first * SPRITE_SIZE);
}

Ball::Ball(const Ball& ball2)
{
	SetPosition(ball2.m_position);
	SetBallShow(ball2.m_show);
}

Ball& Ball::operator=(const Ball& ball2)
{
	this->SetPosition(ball2.m_position);
	this->SetBallShow(ball2.m_show);
	return *this;
}

Ball::Ball()
{
	m_show = true;
}


void Ball::SetBallShow(const bool show)
{
	m_show = show;
}

bool Ball::GetBallShow() const
{
	return m_show;
}

Ball::~Ball()
{
}

std::ostream & operator<<(std::ostream & out, const Ball & ball)
{
	if (ball.m_show)
	{
		out << ".";
	}
	return out;
}
