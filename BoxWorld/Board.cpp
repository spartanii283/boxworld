#include "Board.h"

Board::Board(const Board& board)
{
	for (auto i = 0; i < m_generalLine; i++)
		for (auto j = 0; j < m_generalColumn; j++)
			this->m_board[i][j] = board.m_board[i][j];
	this->m_ballsList = board.m_ballsList;
	this->m_boxesList = board.m_boxesList;
	this->m_playerPositionInGame = board.m_playerPositionInGame;
	this->m_currentLevelFormat = board.GetLevelFormat();
}

Board::~Board()
{
}

Board& Board::operator=(const Board& board)
{
	for (auto i = 0; i < board.m_generalLine; i++)
		for (auto j = 0; j < board.m_generalColumn; j++)
		{
			if (std::dynamic_pointer_cast<Wall>(board.m_board[i][j]))
			{
				m_board[i][j] = std::make_shared<Wall>(Elements::Position(i, j));
			}
			else if (std::dynamic_pointer_cast<Box>(board.m_board[i][j]))
			{
				m_board[i][j] = std::make_shared<Box>(Elements::Position(i, j));
			}
			else if (std::dynamic_pointer_cast<Player>(board.m_board[i][j]))
			{
				m_board[i][j] = std::make_shared<Player>(Elements::Position(i, j));
			}
			else if (std::dynamic_pointer_cast<Ball>(board.m_board[i][j]))
			{
				m_board[i][j] = std::make_shared<Ball>(Elements::Position(i, j));
			}
			else if (std::dynamic_pointer_cast<Road>(board.m_board[i][j]))
			{
				m_board[i][j] = std::make_shared<Road>(Elements::Position(i, j));
			}
		}
	m_playerPositionInGame = board.m_playerPositionInGame;
	m_currentLevelFormat.m_levelNumber = board.m_currentLevelFormat.m_levelNumber;
	return *this;
}

void Board::SetLevelFormat(uint16_t levelNr)
{
	std::string levelName;
	levelName = "Levels/level";
	levelName.append(std::to_string(levelNr));
	levelName.append(".txt");

	m_currentLevelFormat.SetFileName(levelName);

	std::vector<std::string> levelType = m_currentLevelFormat.ScanLevelFromFile();
	std::vector<Elements::Position> boxes;
	std::vector<Elements::Position> balls;
	std::vector<Elements::Position> walls;
	std::vector<Elements::Position> paths;
	m_currentLevelFormat.SetInitialPositions(levelType);
	boxes = m_currentLevelFormat.GetInitialPositionBoxes();
	balls = m_currentLevelFormat.GetInitialPositionBalls();
	walls = m_currentLevelFormat.GetInitialPositionWalls();
	paths = m_currentLevelFormat.GetInitialPositionPaths();
	Elements::Position playerPosition;
	playerPosition = m_currentLevelFormat.GetInitialPostionPlayer();
	for (auto& boxPosition : boxes)
	{
		m_board[boxPosition.first][boxPosition.second] = std::make_shared<Box>(boxPosition);
	}
	for (auto& ballPosition : balls)
	{
		m_board[ballPosition.first][ballPosition.second] = std::make_shared<Ball>(ballPosition);
	}
	for (auto& wallPosition : walls)
	{
		m_board[wallPosition.first][wallPosition.second] = std::make_shared<Wall>(wallPosition);
	}
	for (auto& pathPosition : paths)
	{
		m_board[pathPosition.first][pathPosition.second] = std::make_shared<Road>(pathPosition);
	}
	m_board[playerPosition.first][playerPosition.second] = std::make_shared<Player>(playerPosition);
	m_playerPositionInGame = playerPosition;
}

Level Board::GetLevelFormat()const
{
	return m_currentLevelFormat;
}


uint16_t Board::GetLevelNumber() const
{
	return m_currentLevelFormat.m_levelNumber;
}

void Board::RenderSprites(sf::RenderWindow* window)
{
	for (auto i = 0; i < m_generalLine; i++)
	{
		for (auto j = 0; j < m_generalColumn; j++)
		{
			window->draw(*m_board[i][j]);
		}
	}
}

Elements::Position Board::GetPositionPlayerInGame()
{
	return m_currentLevelFormat.GetInitialPostionPlayer();
}

void Board::GetBoxes()
{
	std::vector<std::shared_ptr<Elements>> boxesListAuxiliar;
	uint8_t index = 0;
	for (auto lineIt : m_board)
	{
		for (auto columnIt : m_board[index])
		{
			if (std::dynamic_pointer_cast<Box>(columnIt))
			{
				boxesListAuxiliar.emplace_back(columnIt);
			}
		}
		++index;
	}
	m_boxesList = boxesListAuxiliar;
}

void Board::GetBalls()
{
	std::vector<std::shared_ptr<Elements>> ballsListAuxiliar;
	uint8_t index = 0;
	for (auto lineIt : m_board)
	{
		for (auto columnIt : m_board[index])
		{
			if (std::dynamic_pointer_cast<Ball>(columnIt))
			{
				ballsListAuxiliar.emplace_back(columnIt);
			}
		}
		++index;
	}
	m_ballsList = ballsListAuxiliar;
}

std::shared_ptr<Elements> Board::GetElement(Elements::Position position)
{
	return m_board[position.first][position.second];
}

void Board::DirectBoxAndPlayer(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));
	auto position2 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + (directionAdd.first * 2)), static_cast<uint8_t>(player.GetPosition().second + (directionAdd.second * 2)));

	SwapBoardElements(position2, position1);
	SwapBoardElements(player.GetPosition(), position1);
	m_board[position2.first][position2.second]->setColor(sf::Color::Yellow);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::DirectBoxAndPlayerOnBall(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));
	auto position2 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + (directionAdd.first * 2)), static_cast<uint8_t>(player.GetPosition().second + (directionAdd.second * 2)));
	std::shared_ptr<Elements> auxiliarRoad = std::make_shared<Road>(position2);

	SwapElements(auxiliarRoad, m_board[position2.first][position2.second], position2, position2);
	SwapBoardElements(position2, position1);
	SwapBoardElements(player.GetPosition(), position1);
	m_board[position2.first][position2.second]->setColor(sf::Color::Red);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::DirectBoxAndPlayerOnBallBallUnder(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));
	auto position2 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + (directionAdd.first * 2)), static_cast<uint8_t>(player.GetPosition().second + (directionAdd.second * 2)));
	std::shared_ptr<Elements> auxiliarRoad = std::make_shared<Road>(position2);

	SwapElements(auxiliarRoad, m_board[position2.first][position2.second], position2, position2);
	SwapBoardElements(position2, position1);
	m_board[position1.first][position1.second] = m_ballsList[SearchBallInList(player.GetPosition())];
	SwapBoardElements(player.GetPosition(), position1);
	m_board[position2.first][position2.second]->setColor(sf::Color::Red);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::DirectPlayer(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));

	SwapBoardElements(player.GetPosition(), position1);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::DirectPlayerOnBall(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));
	std::shared_ptr<Elements> auxiliarRoad = std::make_shared<Road>(player.GetPosition());

	SwapElements(auxiliarRoad, m_board[position1.first][position1.second], position1, position1);
	SwapBoardElements(player.GetPosition(), position1);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::DirectPlayerOnBallBallUnder(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));
	std::shared_ptr<Elements> auxiliarRoad = std::make_shared<Road>();

	SwapElements(auxiliarRoad, m_board[position1.first][position1.second], position1, position1);
	m_board[position1.first][position1.second] = m_ballsList[SearchBallInList(player.GetPosition())];
	SwapBoardElements(player.GetPosition(), position1);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::DirectPlayerBallUnder(Player& player)
{
	Elements::Position directionAdd = player.GetDirection();
	auto position1 = Elements::Position(static_cast<uint8_t>(player.GetPosition().first + directionAdd.first), static_cast<uint8_t>(player.GetPosition().second + directionAdd.second));

	m_board[position1.first][position1.second] = m_ballsList[SearchBallInList(player.GetPosition())];
	SwapBoardElements(player.GetPosition(), position1);
	player.SetPosition(position1);
	player.setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
}

void Board::SwapBoardElements(Elements::Position position1, Elements::Position position2)
{
	std::swap(m_board[position1.first][position1.second], m_board[position2.first][position2.second]);

	m_board[position1.first][position1.second]->SetPosition(position1);
	m_board[position1.first][position1.second]->setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
	m_board[position2.first][position2.second]->SetPosition(position2);
	m_board[position2.first][position2.second]->setPosition(position2.second * SPRITE_SIZE, position2.first * SPRITE_SIZE);

}

void Board::SwapElements(std::shared_ptr<Elements>& element1, std::shared_ptr<Elements>& element2, Elements::Position position1, Elements::Position position2)
{
	std::swap(element1, m_board[position2.first][position2.second]);

	element1->SetPosition(position1);
	element1->setPosition(position1.second * SPRITE_SIZE, position1.first * SPRITE_SIZE);
	element2->SetPosition(position2);
	element2->setPosition(position2.second * SPRITE_SIZE, position2.first * SPRITE_SIZE);
}

bool Board::PossibleToMove(Player player, Elements::Position directionAdd)
{
	auto row = static_cast<uint8_t>(player.GetPosition().first + directionAdd.first);
	auto row2 = static_cast<uint8_t>(player.GetPosition().first + (directionAdd.first * 2));
	auto column = static_cast<uint8_t>(player.GetPosition().second + directionAdd.second);
	auto column2 = static_cast<uint8_t>(player.GetPosition().second + (directionAdd.second * 2));

	if (std::dynamic_pointer_cast<Wall>(m_board[row][column]))
	{
		return false;
	}
	else
		if ((std::dynamic_pointer_cast<Box>(m_board[row][column])) &&
			(std::dynamic_pointer_cast<Wall>(m_board[row2][column2])))
		{
			return false;
		}
		else
			if ((std::dynamic_pointer_cast<Box>(m_board[row][column])) &&
				(std::dynamic_pointer_cast<Box>(m_board[row2][column2])))
			{
				return false;
			};
	return true;
}

bool Board::IsBallVisible(Elements::Position position)
{
	return std::dynamic_pointer_cast<Ball>(m_board[position.first][position.second])->GetBallShow();
}

bool Board::IsBallInList(Elements::Position position)
{
	for (const auto& ball : m_ballsList)
	{
		if (ball->GetPosition() == position)
		{
			return true;
		}
	}
	return false;
}

void Board::SetVisible(Elements::Position position)
{
	auto ball = std::dynamic_pointer_cast<Ball>(m_board[position.first][position.second]);
	if (ball) {
		ball->SetBallShow(true);
	}
}

void Board::SetInvisible(Elements::Position position)
{
	std::dynamic_pointer_cast<Ball>(m_board[position.first][position.second])->SetBallShow(false);
}

void Board::SetVisibility(Elements::Position position, bool visibility)
{
	std::dynamic_pointer_cast<Ball>(m_board[position.first][position.second])->SetBallShow(visibility);
}

void Board::InsertBallFromList(Elements::Position position)
{
	m_board[position.first][position.second] = m_ballsList[this->SearchBallInList(position)];
	m_board[position.first][position.second]->SetPosition(position);
	m_board[position.first][position.second]->setPosition(position.second * SPRITE_SIZE, position.first * SPRITE_SIZE);
}

uint8_t Board::SearchBallInList(Elements::Position position)
{
	uint8_t index = 0;
	for (auto ball : m_ballsList)
	{
		if (ball->GetPosition() == position)
			return index;
		index++;
	}
	return index;
}

Elements::Position Board::GetPlayerPositionInGame()
{
	return m_playerPositionInGame;
}

std::shared_ptr<Elements>& Board::operator[](const Elements::Position& position)
{
	const auto& [line, column] = position;
	return m_board[line][column];
}


const std::shared_ptr<Elements>& Board::operator[](const Elements::Position& position) const
{
	const auto& [line, column] = position;
	return m_board[line][column];

}

Board::State Board::Check()
{
	int ballsAndBoxes = 0;
	bool isGameLost = false;

	for (auto index = 0; index < m_boxesList.size(); index++)
	{
		if (IsBallInList(m_boxesList[index]->GetPosition()))
		{
			ballsAndBoxes++;
		}
		if (!IsBallInList(m_boxesList[index]->GetPosition()))
		{
			if (CheckBoxState({ m_boxesList[index]->GetPosition().first + 1,m_boxesList[index]->GetPosition().second }) && CheckBoxState({ m_boxesList[index]->GetPosition().first, m_boxesList[index]->GetPosition().second - 1 })
				|| CheckBoxState({ m_boxesList[index]->GetPosition().first + 1,m_boxesList[index]->GetPosition().second }) && CheckBoxState({ m_boxesList[index]->GetPosition().first, m_boxesList[index]->GetPosition().second + 1 })
				|| CheckBoxState({ m_boxesList[index]->GetPosition().first - 1,m_boxesList[index]->GetPosition().second }) && CheckBoxState({ m_boxesList[index]->GetPosition().first, m_boxesList[index]->GetPosition().second - 1 })
				|| CheckBoxState({ m_boxesList[index]->GetPosition().first - 1,m_boxesList[index]->GetPosition().second }) && CheckBoxState({ m_boxesList[index]->GetPosition().first,m_boxesList[index]->GetPosition().second + 1 }))
			{
				isGameLost = true;
			}
		}
	}
	if (ballsAndBoxes == m_boxesList.size())
		return State::Win;
	if (isGameLost)
		return State::Lose;
	return State::None;
}

bool Board::CheckBoxState(Elements::Position nextPosition)
{
	const auto& [line, column] = nextPosition;
	if (std::dynamic_pointer_cast<Wall>(m_board[line][column]))
	{
		return true;
	}
	return false;
}

std::ostream& operator<<(std::ostream& out, Board& board)
{
	for (auto i = 0; i < board.m_generalLine; i++)
	{
		for (auto j = 0; j < board.m_generalColumn; j++)
		{
			if (std::dynamic_pointer_cast<Wall>(board.m_board[i][j]))
			{
				out << *(Wall*)board.m_board[i][j].get();
			}
			else if (std::dynamic_pointer_cast<Box>(board.m_board[i][j]))
			{
				out << *(Box*)board.m_board[i][j].get();
			}
			else if (std::dynamic_pointer_cast<Player>(board.m_board[i][j]))
			{
				out << *(Player*)board.m_board[i][j].get();
			}
			else if (std::dynamic_pointer_cast<Ball>(board.m_board[i][j]))
			{
				out << *(Ball*)board.m_board[i][j].get();
			}
			else
				out << " ";
		}
		out << "\n";
	}
	std::cout.flush();


	return out;
}

