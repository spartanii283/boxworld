#pragma once

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Board.h"
#include "Player.h"
#include "PlayerData.h"
#include "../Logging/Logger.h"
#include "SFML/System.hpp"
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "SFML/Network.hpp"

class Game
{
private:
	//Variables
	Board m_board;
	Player m_player;
	PlayerData m_playerData;

	sf::RenderWindow* window;
	sf::RenderWindow* menu;
	sf::Event sfEvent;
	sf::SoundBuffer m_moveSound;
	sf::Sound m_sound;
	sf::Font m_font;
	sf::Clock deltaTimeClook;
	float deltaTime;
	//Initialization
	void InitWindow();
	
public:
	Game();
	~Game();

	//Functions
	void Movement();
	void UpdateDeltaTime();
	void UpdateSfmlEvents(Board boardBackUp, bool& menuIsOpen);
	void Update(Board boardBackUp, bool& menuIsOpen);
	void Render();
	void Run();
	void Menu(bool& menuIsOpen, Board& backUp);
	void SetMoveSound();
	void GameLost(bool& gameLost, Board& boardBackUp);
	void GameWon(bool& gameWon, Board& backUp);
	void Reset(Board boardBackUp);
	void SetFont();
	void Levels(bool setLevelsOn, Board& backUp);
	sf::Text Level(sf:: Text levelName, uint16_t levelNumber);
};

