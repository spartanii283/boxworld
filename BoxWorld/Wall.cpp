#include "Wall.h"

Wall::Wall(Position position): Elements(position)
{
	if (!m_wallTexture.loadFromFile("../Resources/Wall.png"))
	{
		std::cout << "Error could not load wall image" << std::endl;
	}

	this->setTexture(m_wallTexture);
	this->setPosition(position.second * SPRITE_SIZE, position.first * SPRITE_SIZE);
}

Wall::Wall()
{
}

Wall::~Wall()
{
}

std::ostream& operator<<(std::ostream& out, const Wall& ball)
{
	out << "#";
	return out;
}
