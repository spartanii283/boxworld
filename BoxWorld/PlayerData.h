#pragma once
#include <string>
#include <array>
#include <cstdint>
#include <fstream>
#include "Constants.h"

class PlayerData
{
public:
	using BestScore = std::pair<uint16_t, uint16_t>;

public:
	PlayerData(std::array <BestScore, NUMBER_OF_LEVELS> bestScore, uint16_t lastLevel = 0);
	PlayerData();

	//best score
	BestScore GetBestScoreOnLevel(uint16_t level) const;
	void SetBestScore(BestScore bestScore, uint16_t level);
	bool IsBestScore(uint16_t level);

	//playerMoves
	uint16_t GetPlayerMoves()const;
	void  SetPlayerMoves(uint16_t playerMoves);

	//boxMoves
	uint16_t GetBoxMoves()const;
	void  SetBoxMoves(uint16_t boxMoves);

	//level
	uint16_t GetLastLevel() const;
	void SetLastLevel(uint16_t lastLevel);

private:
	uint16_t m_playerMoves;
	uint16_t m_boxMoves;
	uint16_t m_lastLevel;
	std::array <BestScore, NUMBER_OF_LEVELS> m_bestScore;
};

