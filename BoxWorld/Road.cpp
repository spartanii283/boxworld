#include "Road.h"



Road::Road()
{
}

Road::Road(Position position) : Elements(position)
{
	if (!m_roadTexture.loadFromFile("../Resources/Road.png"))
	{
		std::cout << "Error could not load road image" << std::endl;
	}

	this->setTexture(m_roadTexture);
	this->setPosition(position.second * SPRITE_SIZE, position.first * SPRITE_SIZE);
}


Road::~Road()
{
}

std::ostream & operator<<(std::ostream & out, const Road & road)
{
	out << " ";
	return out;
}
