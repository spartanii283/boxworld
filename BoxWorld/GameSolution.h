#pragma once
#include "Board.h"
#include "Elements.h"
#include <string>
#include <queue>
#include <unordered_map>
#include <stack>

class GameBoard
{
public:
	GameBoard();
	Board board;
	GameBoard *previous;
	uint16_t price;
	uint16_t pos1;
	uint16_t pos2;
	std::string move;
	Player player;
	long long code;
};
class GameSolution
{
public:
	void GetSolution(Board board);
	GameBoard GraphSearch();
	bool GoalTest(GameBoard gameBoard);
	bool Validation(GameBoard gameBoard, long long code);
	void Expand( GameBoard gameBoard);
	void SetBoard(GameBoard& boardG, Board board, GameBoard* prev, uint16_t price, uint16_t posFirst, uint16_t posSecond, const std::string& move, long long code);
private:
	std::queue <GameBoard> m_queue;
	std::unordered_map<long long,GameBoard> m_hash;
	std::stack<GameBoard> m_stack;
};
 