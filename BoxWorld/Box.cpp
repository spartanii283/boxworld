#include "Box.h"

Box::Box(Position position) : Elements(position), m_color(false)
{
	if (!m_boxTexture.loadFromFile("../Resources/EmptyBox.png"))
	{
		std::cout << "Error could not load box image" << std::endl;
	}

	this->setTexture(m_boxTexture);
	this->setPosition(position.second * SPRITE_SIZE, position.first * SPRITE_SIZE);
}

Box::Box()
{
}

Box::Box(const Box& box2)
{
	SetPosition(box2.m_position);
	SetBoxColor(box2.m_color);
}

Box& Box::operator=(const Box& box2)
{
	this->SetPosition(box2.m_position);
	this->SetBoxColor(box2.m_color);
	return *this;
}

void Box::SetBoxColor(bool color)
{
	m_color = color;
}

bool Box::GetBoxColor() const
{
	return m_color;
}

std::array<std::pair<uint8_t, uint8_t>, 4> Box::GetAvailableBoxMoves() const
{
	return m_availableBoxMoves;
}

Box::~Box()
{
}

std::ostream& operator<<(std::ostream& out, const Box& box)
{
	if (box.m_color == true)
		out << "*";
	else
		out << "$";
	return out;
}
