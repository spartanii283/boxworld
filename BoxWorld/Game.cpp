#include "Game.h"

std::ofstream logFile("log.log", std::ios::app);
Logger logger(std::cout, Logger::Level::Info);

//Initialization
void Game::InitWindow()
{
	//Creates a SFML window
	this->window = new sf::RenderWindow(sf::VideoMode(700, 700), "Box World");
}

Game::Game()
{
	SetMoveSound();
	this->InitWindow();
}

Game::~Game()
{
	delete this->window;
	delete this->menu;
}

void Game::Movement()
{
	Elements::Position playerProximity = Elements::Position(m_player.GetPosition().first + m_player.GetDirection().first, m_player.GetPosition().second + m_player.GetDirection().second);
	Elements::Position doublePlayerProximity = Elements::Position(m_player.GetPosition().first + m_player.GetDirection().first * 2, m_player.GetPosition().second + m_player.GetDirection().second * 2);
	if (m_board.PossibleToMove(m_player, m_player.GetDirection()))
	{
		m_sound.setBuffer(m_moveSound);
		m_sound.play();
		m_playerData.SetPlayerMoves(m_playerData.GetPlayerMoves() + 1);

		//caz player +- cutie 
		if (m_board.IsElementCollision<Box>(playerProximity))
		{
			m_playerData.SetBoxMoves(m_playerData.GetBoxMoves() + 1);
			//caz player + cutie +- bila
			if (!m_board.IsElementCollision<Ball>(doublePlayerProximity))
			{
				//caz player + cutie +- bila sub player
				if (m_board.IsBallInList(m_player.GetPosition()))
				{
					m_board.DirectBoxAndPlayer(m_player);
					m_board.InsertBallFromList(Elements::Position(m_player.GetPosition().first - m_player.GetDirection().first, m_player.GetPosition().second - m_player.GetDirection().second));
					m_board.SetVisible(Elements::Position(m_player.GetPosition().first + m_player.GetDirection().first, m_player.GetPosition().second + m_player.GetDirection().second));
				}
				else
				{
					m_board.DirectBoxAndPlayer(m_player);
				}
			}
			else
			{
				// caz player + bila +- bila sub player
				if (!m_board.IsBallInList(m_player.GetPosition()))
				{
					m_board.SetInvisible(doublePlayerProximity);
					m_board.DirectBoxAndPlayerOnBall(m_player);
				}
				else
				{
					m_board.SetInvisible(doublePlayerProximity);
					m_board.DirectBoxAndPlayerOnBallBallUnder(m_player);
					m_board.SetVisible(Elements::Position(m_player.GetPosition().first + m_player.GetDirection().first, m_player.GetPosition().second + m_player.GetDirection().second));
				}
			}
		}
		else
			//caz player +- bila 
			if (m_board.IsElementCollision<Ball>(playerProximity))
			{
				//caz player + bila +- bila sub player
				if (!m_board.IsBallInList(m_player.GetPosition()))
				{
					m_board.SetInvisible(playerProximity);
					m_board.DirectPlayerOnBall(m_player);
				}
				else
				{
					m_board.SetInvisible(playerProximity);
					m_board.DirectPlayerOnBallBallUnder(m_player);
					m_board.SetVisible(Elements::Position(m_player.GetPosition().first + m_player.GetDirection().first, m_player.GetPosition().second + m_player.GetDirection().second));
				}
			}
			else
			{
				//caz player +- bila sub player
				if (!m_board.IsBallInList(m_player.GetPosition()))
				{
					m_board.DirectPlayer(m_player);
				}
				else
				{
					m_board.DirectPlayerBallUnder(m_player);
					m_board.SetVisible(Elements::Position(m_player.GetPosition().first + m_player.GetDirection().first, m_player.GetPosition().second + m_player.GetDirection().second));
				}
			}
		this->Render();
	}
}

void Game::UpdateDeltaTime()
{
	//Update deltaTime variable with the time it takes to update and render one frame
	this->deltaTime = this->deltaTimeClook.restart().asSeconds();
}

void Game::UpdateSfmlEvents(Board boardBackUp, bool& menuIsOpen)
{
	while (this->window->pollEvent(this->sfEvent) && !menuIsOpen)
	{
		switch (this->sfEvent.type) {
		case sf::Event::Closed:
		{
			logger.Logi(Logger::Level::Info, "Game closed");
			this->window->close();
			break;
		}
		case sf::Event::KeyPressed:
		{
			switch (this->sfEvent.key.code)
			{
			case sf::Keyboard::Up:
			{
				m_player.SetDirection(DIR_UP);

				//logger message
				logger.Logi(Logger::Level::Debug, "Player moved upwards");

				Movement();
				break;
			}
			case sf::Keyboard::Down:
			{
				m_player.SetDirection(DIR_DOWN);

				//logger message
				logger.Logi(Logger::Level::Debug, "Player moved downwards");

				Movement();
				break;
			}
			case sf::Keyboard::Left:
			{
				m_player.SetDirection(DIR_LEFT);

				//logger message
				logger.Logi(Logger::Level::Debug, "Player moved to the left");

				Movement();
				break;
			}
			case sf::Keyboard::Right:
			{
				m_player.SetDirection(DIR_RIGHT);

				//logger message
				logger.Logi(Logger::Level::Debug, "Player moved to the right");

				Movement();
				break;
			}
			case sf::Keyboard::R:
			{
				logger.Logi(Logger::Level::Info, "Current level was restarted");
				Reset(boardBackUp);
				break;
			}
			case sf::Keyboard::M:
			{
				menuIsOpen = true;
				break;
			}

			}
		}

		}
	}
}

void Game::Update(Board boardBackUp, bool& menuIsOpen)
{
	this->UpdateSfmlEvents(boardBackUp, menuIsOpen);
}

void Game::Render()
{
	this->window->clear();

	m_board.RenderSprites(window);

	this->window->display();
}

void Game::Run()
{
	//logger message
	logger.Logi(Logger::Level::Info, "Game started");
	m_board.SetLevelFormat(1);
	m_player.SetPosition(m_board.GetPlayerPositionInGame());
	m_board.GetBalls();
	m_board.GetBoxes();
	Board boardBackUp;
	bool gameLost = true;
	bool gameWon = true;
	bool menuIsOpen = true;
	while (this->window->isOpen())
	{
		if (menuIsOpen)
		{
			this->Menu(menuIsOpen, boardBackUp);
			boardBackUp = m_board;
			Reset(boardBackUp);
		}
		else if (gameLost && m_board.Check() == Board::State::Lose)
		{
			this->GameLost(gameLost, boardBackUp);
			gameLost = true;
		}
		else if (gameWon && m_board.Check() == Board::State::Win)
		{
			this->GameWon(gameWon, boardBackUp);
			gameWon = true;
		}
		else
		{
			this->UpdateDeltaTime();
			this->Update(boardBackUp, menuIsOpen);
			this->Render();
		}
	}
}

void Game::Menu(bool& menuIsOpen, Board& backUp)
{
	logger.Logi(Logger::Level::Info, "Start menu");
	//Imaginea de fundal din meniu
	sf::Texture menuTexture;
	menuTexture.loadFromFile("../Resources/Menu.png");
	if (!menuTexture.loadFromFile("../Resources/Menu.png"))
		logger.Logi(Logger::Level::Error, "Menu.png cannot open");
	sf::Sprite sprite;
	sprite.setTexture(menuTexture);
	//Textul pentru butoanele din meniu
	SetFont();
	sf::Text start;
	sf::Text exit;
	start.setFont(m_font);
	exit.setFont(m_font);
	start.setString("Start");
	exit.setString("Exit");
	start.setPosition(250.0f, 415.0f);
	exit.setPosition(267.0f, 490.0f);
	start.setCharacterSize(77);
	exit.setCharacterSize(77);
	start.setFillColor(sf::Color::Red);
	exit.setFillColor(sf::Color::Yellow);
	bool startOn = true;
	while (this->window->isOpen() && menuIsOpen)
	{
		this->UpdateDeltaTime();
		while (this->window->pollEvent(this->sfEvent))
		{
			switch (this->sfEvent.type)
			{
			case sf::Event::Closed:
			{
				logger.Logi(Logger::Level::Info, "Game closed");
				this->window->close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				switch (this->sfEvent.key.code)
				{

				case sf::Keyboard::Up:
				{
					if (startOn)
					{
						start.setFillColor

						(sf::Color::Yellow);
						exit.setFillColor

						(sf::Color::Red);
						startOn = false;
					}
					else
					{
						start.setFillColor

						(sf::Color::Red);
						exit.setFillColor

						(sf::Color::Yellow);
						startOn = true;
					}
					break;
				}
				case sf::Keyboard::Down:
				{
					if (startOn)
					{
						start.setFillColor

						(sf::Color::Yellow);
						exit.setFillColor

						(sf::Color::Red);
						startOn = false;
					}
					else
					{
						start.setFillColor

						(sf::Color::Red);
						exit.setFillColor

						(sf::Color::Yellow);
						startOn = true;
					}
					break;
				}
				case sf::Keyboard::Enter:
				{
					if (startOn)
					{
						menuIsOpen = false;
						bool setLevelsOn = true;
						Levels(setLevelsOn, backUp);
					}
					else
					{
						logger.Logi(Logger::Level::Info, "Game closed");
						this->window->close();
					}
					break;
				}
				}
				break;
			}
			}
		}
		this->window->clear();
		this->window->draw(sprite);
		this->window->draw(start);
		this->window->draw(exit);
		this->window->display();
	}
}

void Game::SetMoveSound()
{
	m_moveSound.loadFromFile("../Resources/sfx_point.wav");
}

void Game::GameLost(bool& gameLost, Board& boardBackUp)
{
	logger.Logi(Logger::Level::Info, "Game lost");
	//sprite GameLost
	sf::Texture menuTexture2;
	menuTexture2.loadFromFile("../Resources/GameLost.png");
	if (!menuTexture2.loadFromFile("../Resources/GameLost.png"))
		logger.Logi(Logger::Level::Error, "GameLost.png cannot open");
	sf::Sprite sprite;
	sprite.setTexture(menuTexture2);
	sf::Text restartLevel;
	sf::Text exit;
	sf::Text pickAnotherLevel;

	bool restartOn = true;
	bool levelOn = true;
	//buttons customization
	restartLevel.setFont(m_font);
	exit.setFont(m_font);
	pickAnotherLevel.setFont(m_font);
	restartLevel.setString("Restart Level");
	exit.setString("Exit");
	pickAnotherLevel.setString("Pick  Another  Level");
	restartLevel.setPosition(250.0f, 415.0f);
	exit.setPosition(315.0f, 490.0f);
	pickAnotherLevel.setPosition(220.0f, 350.0f);
	restartLevel.setCharacterSize(30);
	exit.setCharacterSize(30);
	pickAnotherLevel.setCharacterSize(30);
	restartLevel.setFillColor(sf::Color::Red);
	exit.setFillColor(sf::Color::Yellow);
	pickAnotherLevel.setFillColor(sf::Color::Yellow);
	while (this->window->isOpen() && gameLost)
	{
		this->UpdateDeltaTime();
		while (this->window->pollEvent(this->sfEvent))
		{
			switch (this->sfEvent.type)
			{
			case sf::Event::Closed:
			{
				logger.Logi(Logger::Level::Info, "Game closed");
				this->window->close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				switch (this->sfEvent.key.code)
				{
				case sf::Keyboard::Escape:
				{
					logger.Logi(Logger::Level::Info, "Game closed");
					this->window->close();
					break;
				}
				case sf::Keyboard::Down:
				{
					if (restartOn)
					{
						restartLevel.setFillColor(sf::Color::Yellow);
						pickAnotherLevel.setFillColor(sf::Color::Yellow);
						exit.setFillColor(sf::Color::Red);
						restartOn = false;
						levelOn = false;
					}
					else if (levelOn)
					{
						restartLevel.setFillColor(sf::Color::Red);
						pickAnotherLevel.setFillColor(sf::Color::Yellow);
						exit.setFillColor(sf::Color::Yellow);
						restartOn = true;
						levelOn = false;
					}
					else
					{
						restartLevel.setFillColor(sf::Color::Yellow);
						pickAnotherLevel.setFillColor(sf::Color::Red);
						exit.setFillColor(sf::Color::Yellow);
						restartOn = false;
						levelOn = true;
					}
					break;
				}
				case sf::Keyboard::Up:
				{
					if (restartOn)
					{
						restartLevel.setFillColor(sf::Color::Yellow);
						pickAnotherLevel.setFillColor(sf::Color::Red);
						exit.setFillColor(sf::Color::Yellow);
						restartOn = false;
						levelOn = true;
					}
					else if (levelOn)
					{
						restartLevel.setFillColor(sf::Color::Yellow);
						pickAnotherLevel.setFillColor(sf::Color::Yellow);
						exit.setFillColor(sf::Color::Red);
						restartOn = false;
						levelOn = false;
					}
					else
					{
						restartLevel.setFillColor(sf::Color::Red);
						pickAnotherLevel.setFillColor(sf::Color::Yellow);
						exit.setFillColor(sf::Color::Yellow);
						restartOn = true;
						levelOn = false;
					}
					break;
				}
				case sf::Keyboard::Enter:
				{
					if (restartOn)
					{
						logger.Logi(Logger::Level::Info, "Current level was restarted");
						Reset(boardBackUp);
						gameLost = false;
					}
					else if (levelOn)
					{
						bool setLevelsOn = true;
						gameLost = false;
						Levels(setLevelsOn, boardBackUp);
					}
					else
					{
						logger.Logi(Logger::Level::Info, "Game closed");
						this->window->close();
					}
					break;
				}
				}
				break;
			}
			}
			this->window->clear();
			this->window->draw(sprite);
			this->window->draw(restartLevel);
			this->window->draw(pickAnotherLevel);
			this->window->draw(exit);
			this->window->display();
		}
	}
}

void Game::GameWon(bool& gameWon, Board& backUp)
{
	logger.Logi(Logger::Level::Info, "Level is cleared");
	//sprite GameWon
	sf::Texture menuTexture2;
	menuTexture2.loadFromFile("../Resources/GameWon.png");
	if (!menuTexture2.loadFromFile("../Resources/GameWon.png"))
		logger.Logi(Logger::Level::Error, "GameWon.png cannot open");
	sf::Sprite sprite;
	sprite.setTexture(menuTexture2);
	sf::Text bestScore;
	sf::Text currentScore;
	sf::Text exit;
	sf::Text pickAnotherLevel;

	bool levelsOn = true;
	//buttons customization
	currentScore.setFont(m_font);
	bestScore.setFont(m_font);
	exit.setFont(m_font);
	pickAnotherLevel.setFont(m_font);
	if (m_playerData.IsBestScore(m_board.GetLevelNumber()))
	{
		bestScore.setString("New Best Score  ");
		currentScore.setString("Player  " + std::to_string(m_playerData.GetBestScoreOnLevel(m_board.GetLevelNumber()).first) +
			"  Boxes  " + std::to_string(m_playerData.GetBestScoreOnLevel(m_board.GetLevelNumber()).second));
		bestScore.setPosition(244.0f, 370.0f);
		currentScore.setPosition(219.0f, 405.0f);
	}
	else
	{
		bestScore.setString("Best Score  Player  " + std::to_string(m_playerData.GetBestScoreOnLevel(m_board.GetLevelNumber()).first) +
			"  Boxes  " + std::to_string(m_playerData.GetBestScoreOnLevel(m_board.GetLevelNumber()).second));
		currentScore.setString("Your Moves Player  " + std::to_string(m_playerData.GetPlayerMoves()) +
			"  Boxes  " + std::to_string(m_playerData.GetBoxMoves()));
		bestScore.setPosition(145.0f, 370.0f);
		currentScore.setPosition(145.0f, 405.0f);
	}
	exit.setString("Exit");
	exit.setPosition(318.0f, 550.0f);
	pickAnotherLevel.setString("Pick  Another  Level");
	pickAnotherLevel.setPosition(210.0f, 520);
	bestScore.setCharacterSize(30);
	currentScore.setCharacterSize(30);
	pickAnotherLevel.setCharacterSize(30);
	exit.setCharacterSize(30);
	bestScore.setFillColor(sf::Color::Blue);
	currentScore.setFillColor(sf::Color::Blue);
	pickAnotherLevel.setFillColor(sf::Color::Red);
	exit.setFillColor(sf::Color::Yellow);
	while (this->window->isOpen() && gameWon)
	{
		this->UpdateDeltaTime();
		while (this->window->pollEvent(this->sfEvent))
		{
			switch (this->sfEvent.type)
			{
			case sf::Event::Closed:
			{
				logger.Logi(Logger::Level::Info, "Game closed");
				this->window->close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				switch (this->sfEvent.key.code)
				{

				case sf::Keyboard::Up:
				{
					if (!levelsOn)
					{
						pickAnotherLevel.setFillColor(sf::Color::Red);
						exit.setFillColor(sf::Color::Yellow);
						levelsOn = true;
					}
					else
					{
						pickAnotherLevel.setFillColor(sf::Color::Yellow);
						exit.setFillColor(sf::Color::Red);
						levelsOn = false;
					}
					break;
				}
				case sf::Keyboard::Down:
				{
					if (!levelsOn)
					{
						pickAnotherLevel.setFillColor(sf::Color::Red);
						exit.setFillColor(sf::Color::Yellow);
						levelsOn = true;
					}
					else
					{
						pickAnotherLevel.setFillColor(sf::Color::Yellow);
						exit.setFillColor(sf::Color::Red);
						levelsOn = false;
					}
					break;
				}
				case sf::Keyboard::Enter:
				{
					if (levelsOn)
					{

						bool setLevelsOn = true;
						Levels(setLevelsOn, backUp);
						gameWon = false;
					}
					else
					{
						logger.Logi(Logger::Level::Info, "Game closed");
						this->window->close();
					}
					break;
				}
				}
				break;
			}
			}
		}
		this->window->clear();
		this->window->draw(sprite);
		this->window->draw(currentScore);
		this->window->draw(pickAnotherLevel);
		this->window->draw(bestScore);
		this->window->draw(exit);
		this->window->display();
	}
}

void Game::Reset(Board boardBackUp)
{
	m_board = boardBackUp;
	m_player.SetPosition(m_board.GetPlayerPositionInGame());
	m_board.GetBalls();
	m_board.GetBoxes();
	m_playerData.SetPlayerMoves(0);
	m_playerData.SetBoxMoves(0);
}

void Game::SetFont()
{
	if (!m_font.loadFromFile("../Resources/ARCADECLASSIC.ttf"))
		logger.Logi(Logger::Level::Error, "Cannot open the font");
}

void Game::Levels(bool setLevelsOn, Board& backUp)
{
	logger.Logi(Logger::Level::Info, "Level menu");
	float coordX = 20, coordY = 70;
	bool levelPressed = true;
	uint16_t mmaxLevelsonLine = 5;
	sf::Texture menuTexture2;
	menuTexture2.loadFromFile("../Resources/Levels.png");
	if (!menuTexture2.loadFromFile("../Resources/Levels.png"))
		logger.Logi(Logger::Level::Error, "Levels.png cannot open");
	sf::Sprite sprite;
	sprite.setTexture(menuTexture2);
	sf::Text exit;
	bool restartOn = true;
	//buttons customization
	std::vector<bool> levelChosen;
	std::vector<sf::Text> levels;
	sf::Text foo;
	for (uint16_t i = 0; i < NUMBER_OF_LEVELS; i++)
	{
		levels.emplace_back(foo);
		levels.at(i) = Level(levels.at(i), i + 1);
		if (i % 5 == 0)
		{
			coordX = 20;
			coordY += 70;
		}
		levels.at(i).setPosition(coordX, coordY);
		coordX += 140;
		levelChosen.emplace_back(false);
	}
	exit.setFont(m_font);
	exit.setString("Exit");
	exit.setPosition(318.0f, 535.0f);
	exit.setCharacterSize(30);
	levels.at(0).setFillColor(sf::Color::Red);
	levelChosen.at(0) = true;
	exit.setFillColor(sf::Color::Yellow);
	while (this->window->isOpen() && setLevelsOn)
	{
		this->UpdateDeltaTime();
		while (this->window->pollEvent(this->sfEvent))
		{
			switch (this->sfEvent.type)
			{
			case sf::Event::Closed:
			{
				logger.Logi(Logger::Level::Info, "Game closed");
				this->window->close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				switch (this->sfEvent.key.code)
				{
				case sf::Keyboard::Escape:
				{
					logger.Logi(Logger::Level::Info, "Game closed");
					this->window->close();
					break;
				}
				case sf::Keyboard::Left:
				{
					if (levelPressed)
						for (auto i = 0; i < levelChosen.size(); i++)
							if (levelChosen.at(i))
							{
								if (i - 1 >= 0)
								{
									levels.at(i - 1).setFillColor(sf::Color::Red);
									levels.at(i).setFillColor(sf::Color::Yellow);
									exit.setFillColor(sf::Color::Yellow);
									levelChosen.at(i) = false;
									levelChosen.at(i - 1) = true;
								}
								else
								{
									levels.at(i).setFillColor(sf::Color::Yellow);
									exit.setFillColor(sf::Color::Red);
									levelChosen.at(i) = false;
									levelPressed = false;
								}
							}
							else
							{
								levels.at(i).setFillColor(sf::Color::Yellow);
								levelChosen.at(i) = false;
							}
					else
					{
						levels.at(levels.size() - 1).setFillColor(sf::Color::Red);
						exit.setFillColor(sf::Color::Yellow);
						levelChosen.at(levels.size() - 1) = true;
						levelPressed = true;
					}

					break;
				}
				case sf::Keyboard::Right:
				{
					if (levelPressed)
						for (auto i = 0; i < levelChosen.size(); i++)
							if (levelChosen.at(i))
							{
								if (i + 1 < levelChosen.size())
								{
									levels.at(i + 1).setFillColor(sf::Color::Red);
									levels.at(i).setFillColor(sf::Color::Yellow);
									exit.setFillColor(sf::Color::Yellow);
									levelChosen.at(i) = false;
									levelChosen.at(i + 1) = true;
									break;
								}
								else
								{
									levels.at(i).setFillColor(sf::Color::Yellow);
									exit.setFillColor(sf::Color::Red);
									levelChosen.at(i) = false;
									levelPressed = false;
									break;
								}
							}
							else
							{
								levels.at(i).setFillColor(sf::Color::Yellow);
								levelChosen.at(i) = false;
							}
					else
					{
						levels.at(0).setFillColor(sf::Color::Red);
						exit.setFillColor(sf::Color::Yellow);
						levelChosen.at(0) = true;
						levelPressed = true;
					}
					break;
				}
				case sf::Keyboard::Enter:
				{
					bool levelsOn = false;
					Board newBoard;
					for (auto i = 0; i < NUMBER_OF_LEVELS; i++)
						if (levelChosen.at(i))
						{
							newBoard.SetLevelFormat(i + 1);
							newBoard.GetBalls();
							newBoard.GetBoxes();
							Reset(newBoard);
							backUp = newBoard;
							setLevelsOn = false;
							levelsOn = true;
						}
					if (!levelsOn)
					{
						logger.Logi(Logger::Level::Info, "Game closed");
						this->window->close();
					}
					break;
				}
				}
				break;
			}
			}
			this->window->clear();
			this->window->draw(sprite);
			for (auto i : levels) this->window->draw(i);
			this->window->draw(exit);
			this->window->display();
		}
	}
}

sf::Text Game::Level(sf::Text levelName, uint16_t levelNr)
{
	std::string level;
	level = "Level ";
	level.append(std::to_string(levelNr));
	levelName.setFont(m_font);
	levelName.setString(level);
	levelName.setCharacterSize(30);
	levelName.setFillColor(sf::Color::Yellow);
	return levelName;
}


