#include "GameSolution.h"
GameBoard::GameBoard()
{
}
void GameSolution::GetSolution(Board board)
{
	GameBoard initial, solution;
	SetBoard(initial, board, nullptr, 0, NULL, NULL, "", 0);
	m_queue.emplace(initial);
	solution = GraphSearch();
}

GameBoard GameSolution::GraphSearch()
{
	GameBoard auxiliar;
	bool fail = false;
	long long code = 0;
	while (!GoalTest(m_queue.front()))
	{
		if (m_queue.empty())
		{
			GameBoard* fail;
			fail = nullptr;
			return *fail;
		}
		auxiliar = m_queue.front();
		m_queue.pop();
		code++;
		auxiliar.code = code;
		if (!Validation(auxiliar, code))
		{
			m_hash[code] = auxiliar;
			Expand(auxiliar);
		}
	}
	return m_queue.front();
}

bool GameSolution::GoalTest(GameBoard gameBoard)
{
	if (gameBoard.board.Check() == Board::State::Win)
		return true;
	return false;
}

bool GameSolution::Validation(GameBoard gameBoard, long long code)
{
	std::unordered_map<long long, GameBoard>::const_iterator got = m_hash.find(code);
	if (got == m_hash.end())
		return false;
	else
		return true;

}

void GameSolution::Expand(GameBoard gameBoard)
{
	GameBoard newBoardUP;
	SetBoard(newBoardUP, gameBoard.board, &gameBoard, gameBoard.price + 1, -1, 0, "up", gameBoard.code);
	//sus
	Elements::Position playerProximity = Elements::Position(newBoardUP.player.GetPosition().first - 1, newBoardUP.player.GetPosition().second);
	Elements::Position doublePlayerProximity = Elements::Position(newBoardUP.player.GetPosition().first - 2, newBoardUP.player.GetPosition().second);
	if (newBoardUP.board.PossibleToMove(newBoardUP.player, newBoardUP.player.GetDirection()))
	{
		//caz player +- cutie 
		if (newBoardUP.board.IsElementCollision<Box>(playerProximity))
		{
			//caz player + cutie +- bila
			if (!newBoardUP.board.IsElementCollision<Ball>(doublePlayerProximity))
			{
				//caz player + cutie +- bila sub player
				if (newBoardUP.board.IsBallInList(newBoardUP.player.GetPosition()))
				{
					newBoardUP.board.DirectBoxAndPlayer(newBoardUP.player);
					newBoardUP.board.InsertBallFromList(Elements::Position(newBoardUP.player.GetPosition().first + 1, newBoardUP.player.GetPosition().second));
					newBoardUP.board.SetVisible(Elements::Position(newBoardUP.player.GetPosition().first + 1, newBoardUP.player.GetPosition().second));
				}
				else
				{
					newBoardUP.board.DirectBoxAndPlayer(newBoardUP.player);
				}

			}
			else
			{
				// caz player + bila +- bila sub player
				if (!newBoardUP.board.IsBallInList(newBoardUP.player.GetPosition()))
				{
					newBoardUP.board.SetInvisible(doublePlayerProximity);
					newBoardUP.board.DirectBoxAndPlayerOnBall(newBoardUP.player);
				}
				else
				{
					newBoardUP.board.SetInvisible(doublePlayerProximity);
					newBoardUP.board.DirectBoxAndPlayerOnBallBallUnder(newBoardUP.player);
					newBoardUP.board.SetVisible(Elements::Position(newBoardUP.player.GetPosition().first + 1, newBoardUP.player.GetPosition().second));
				}
			}
		}
		else
			//caz player +- bila 
			if (newBoardUP.board.IsElementCollision<Ball>(playerProximity))
			{
				//caz player + bila +- bila sub player
				if (!newBoardUP.board.IsBallInList(newBoardUP.player.GetPosition()))
				{
					newBoardUP.board.SetInvisible(playerProximity);
					newBoardUP.board.DirectPlayerOnBall(newBoardUP.player);
				}
				else
				{
					newBoardUP.board.SetInvisible(playerProximity);
					newBoardUP.board.DirectPlayerOnBallBallUnder(newBoardUP.player);
					newBoardUP.board.SetVisible(Elements::Position(newBoardUP.player.GetPosition().first + 1, newBoardUP.player.GetPosition().second));
				}

			}
			else
			{
				//caz player +- bila sub player
				if (!newBoardUP.board.IsBallInList(newBoardUP.player.GetPosition()))
				{
					newBoardUP.board.DirectPlayer(newBoardUP.player);
				}
				else
				{
					newBoardUP.board.DirectPlayerBallUnder(newBoardUP.player);
					newBoardUP.board.SetVisible(Elements::Position(newBoardUP.player.GetPosition().first + 1, newBoardUP.player.GetPosition().second));
				}
			}
		m_queue.push(newBoardUP);
	}
	GameBoard newBoardDOWN;

	//jos
	SetBoard(newBoardDOWN, gameBoard.board, &gameBoard, gameBoard.price + 1, 1, 0, "down", gameBoard.code);
	playerProximity = Elements::Position(newBoardDOWN.player.GetPosition().first + 1, newBoardDOWN.player.GetPosition().second);
	doublePlayerProximity = Elements::Position(newBoardDOWN.player.GetPosition().first + 2, newBoardDOWN.player.GetPosition().second);
	if (newBoardDOWN.board.PossibleToMove(newBoardDOWN.player, newBoardDOWN.player.GetDirection()))
	{
		//caz player +- cutie 
		if (newBoardDOWN.board.IsElementCollision<Box>(playerProximity))
		{
			//caz player + cutie +- bila
			if (!newBoardDOWN.board.IsElementCollision<Ball>(doublePlayerProximity))
			{
				//caz player + cutie +- bila sub player
				if (newBoardDOWN.board.IsBallInList(newBoardDOWN.player.GetPosition()))
				{
					newBoardDOWN.board.DirectBoxAndPlayer(newBoardDOWN.player);
					newBoardDOWN.board.InsertBallFromList(Elements::Position(newBoardDOWN.player.GetPosition().first - 1, newBoardDOWN.player.GetPosition().second));
					newBoardDOWN.board.SetVisible(Elements::Position(newBoardDOWN.player.GetPosition().first - 1, newBoardDOWN.player.GetPosition().second));
				}
				else
				{
					newBoardDOWN.board.DirectBoxAndPlayer(newBoardDOWN.player);
				}

			}
			else
			{
				// caz player + bila +- bila sub player
				if (!newBoardDOWN.board.IsBallInList(newBoardDOWN.player.GetPosition()))
				{
					newBoardDOWN.board.SetInvisible(doublePlayerProximity);
					newBoardDOWN.board.DirectBoxAndPlayerOnBall(newBoardDOWN.player);
				}
				else
				{
					newBoardDOWN.board.SetInvisible(doublePlayerProximity);
					newBoardDOWN.board.DirectBoxAndPlayerOnBallBallUnder(newBoardDOWN.player);
					newBoardDOWN.board.SetVisible(Elements::Position(newBoardDOWN.player.GetPosition().first - 1, newBoardDOWN.player.GetPosition().second));
				}
			}
		}
		else
			//caz player +- bila 
			if (newBoardDOWN.board.IsElementCollision<Ball>(playerProximity))
			{
				//caz player + bila +- bila sub player
				if (!newBoardDOWN.board.IsBallInList(newBoardDOWN.player.GetPosition()))
				{
					newBoardDOWN.board.SetInvisible(playerProximity);
					newBoardDOWN.board.DirectPlayerOnBall(newBoardDOWN.player);
				}
				else
				{
					newBoardDOWN.board.SetInvisible(playerProximity);
					newBoardDOWN.board.DirectPlayerOnBallBallUnder(newBoardDOWN.player);
					newBoardDOWN.board.SetVisible(Elements::Position(newBoardDOWN.player.GetPosition().first - 1, newBoardDOWN.player.GetPosition().second));
				}

			}
			else
			{
				//caz player +- bila sub player
				if (!newBoardDOWN.board.IsBallInList(newBoardDOWN.player.GetPosition()))
				{
					newBoardDOWN.board.DirectPlayer(newBoardDOWN.player);
				}
				else
				{
					newBoardDOWN.board.DirectPlayerBallUnder(newBoardDOWN.player);
					newBoardDOWN.board.SetVisible(Elements::Position(newBoardDOWN.player.GetPosition().first - 1, newBoardDOWN.player.GetPosition().second));
				}
			}
		m_queue.push(newBoardDOWN);
	}


	GameBoard newBoardLEFT;
	//stanga
	SetBoard(newBoardLEFT, gameBoard.board, &gameBoard, gameBoard.price + 1, 0, -1, "left", gameBoard.code);
	playerProximity = Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second - 1);
	doublePlayerProximity = Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second - 2);
	if (newBoardLEFT.board.PossibleToMove(newBoardLEFT.player, newBoardLEFT.player.GetDirection()))
	{
		//caz player +- cutie 
		if (newBoardLEFT.board.IsElementCollision<Box>(playerProximity))
		{
			//caz player + cutie +- bila
			if (!newBoardLEFT.board.IsElementCollision<Ball>(doublePlayerProximity))
			{
				//caz player + cutie +- bila sub player
				if (newBoardLEFT.board.IsBallInList(newBoardLEFT.player.GetPosition()))
				{
					newBoardLEFT.board.DirectBoxAndPlayer(newBoardLEFT.player);
					newBoardLEFT.board.InsertBallFromList(Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second + 1));
					newBoardLEFT.board.SetVisible(Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second + 1));
				}
				else
				{
					newBoardLEFT.board.DirectBoxAndPlayer(newBoardLEFT.player);
				}

			}
			else
			{
				// caz player + bila +- bila sub player
				if (!newBoardLEFT.board.IsBallInList(newBoardLEFT.player.GetPosition()))
				{
					newBoardLEFT.board.SetInvisible(doublePlayerProximity);
					newBoardLEFT.board.DirectBoxAndPlayerOnBall(newBoardLEFT.player);
					//std::cout << newBoardLEFT.board;
				}
				else
				{
					newBoardLEFT.board.SetInvisible(doublePlayerProximity);
					newBoardLEFT.board.DirectBoxAndPlayerOnBallBallUnder(newBoardLEFT.player);
					newBoardLEFT.board.SetVisible(Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second + 1));
				}
			}
		}
		else
			//caz player +- bila 
			if (newBoardLEFT.board.IsElementCollision<Ball>(playerProximity))
			{
				//caz player + bila +- bila sub player
				if (!newBoardLEFT.board.IsBallInList(newBoardLEFT.player.GetPosition()))
				{
					newBoardLEFT.board.SetInvisible(playerProximity);
					newBoardLEFT.board.DirectPlayerOnBall(newBoardLEFT.player);
				}
				else
				{
					newBoardLEFT.board.SetInvisible(playerProximity);
					newBoardLEFT.board.DirectPlayerOnBallBallUnder(newBoardLEFT.player);
					newBoardLEFT.board.SetVisible(Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second + 1));
				}

			}
			else
			{
				//caz player +- bila sub player
				if (!newBoardLEFT.board.IsBallInList(newBoardLEFT.player.GetPosition()))
				{
					newBoardLEFT.board.DirectPlayer(newBoardLEFT.player);
				}
				else
				{
					newBoardLEFT.board.DirectPlayerBallUnder(newBoardLEFT.player);
					newBoardLEFT.board.SetVisible(Elements::Position(newBoardLEFT.player.GetPosition().first, newBoardLEFT.player.GetPosition().second + 1));
				}
			}
		m_queue.push(newBoardLEFT);
	}

	//right
	GameBoard newBoardRIGHT;
	SetBoard(newBoardRIGHT, gameBoard.board, &gameBoard, gameBoard.price + 1, 0, 1, "right", gameBoard.code);
	playerProximity = Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second + 1);
	doublePlayerProximity = Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second + 2);
	if (newBoardRIGHT.board.PossibleToMove(newBoardRIGHT.player, newBoardRIGHT.player.GetDirection()))
	{
		//caz player +- cutie 
		if (newBoardRIGHT.board.IsElementCollision<Box>(playerProximity))
		{
			//caz player + cutie +- bila
			if (!newBoardRIGHT.board.IsElementCollision<Ball>(doublePlayerProximity))
			{
				//caz player + cutie +- bila sub player
				if (newBoardRIGHT.board.IsBallInList(newBoardRIGHT.player.GetPosition()))
				{
					newBoardRIGHT.board.DirectBoxAndPlayer(newBoardRIGHT.player);
					newBoardRIGHT.board.InsertBallFromList(Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second - 1));
					newBoardRIGHT.board.SetVisible(Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second - 1));
				}
				else
				{
					newBoardRIGHT.board.DirectBoxAndPlayer(newBoardRIGHT.player);
				}

			}
			else
			{
				// caz player +  bila +- bila sub player
				if (!newBoardRIGHT.board.IsBallInList(newBoardRIGHT.player.GetPosition()))
				{
					newBoardRIGHT.board.SetInvisible(doublePlayerProximity);
					newBoardRIGHT.board.DirectBoxAndPlayerOnBall(newBoardRIGHT.player);
				}
				else
				{
					newBoardRIGHT.board.SetInvisible(doublePlayerProximity);
					newBoardRIGHT.board.DirectBoxAndPlayerOnBallBallUnder(newBoardRIGHT.player);
					newBoardRIGHT.board.SetVisible(Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second - 1));
				}
			}
		}
		else
			//caz player +- bila 
			if (newBoardRIGHT.board.IsElementCollision<Ball>(playerProximity))
			{
				//caz player + bila +- bila sub player
				if (!newBoardRIGHT.board.IsBallInList(newBoardRIGHT.player.GetPosition()))
				{
					newBoardRIGHT.board.SetInvisible(playerProximity);
					newBoardRIGHT.board.DirectPlayerOnBall(newBoardRIGHT.player);
				}
				else
				{
					newBoardRIGHT.board.SetInvisible(playerProximity);
					newBoardRIGHT.board.DirectPlayerOnBallBallUnder(newBoardRIGHT.player);
					newBoardRIGHT.board.SetVisible(Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second - 1));
				}

			}
			else
			{
				//caz player +- bila sub player
				if (!newBoardRIGHT.board.IsBallInList(newBoardRIGHT.player.GetPosition()))
				{
					newBoardRIGHT.board.DirectPlayer(newBoardRIGHT.player);
				}
				else
				{
					newBoardRIGHT.board.DirectPlayerBallUnder(newBoardRIGHT.player);
					newBoardRIGHT.board.SetVisible(Elements::Position(newBoardRIGHT.player.GetPosition().first, newBoardRIGHT.player.GetPosition().second - 1));
				}
			}
		m_queue.push(newBoardRIGHT);
	}

}

void GameSolution::SetBoard(GameBoard& boardG, Board board, GameBoard* prev, uint16_t price, uint16_t posFirst, uint16_t posSecond, const std::string& move, long long code)
{
	boardG.board = board;
	boardG.player.SetPosition(boardG.board.GetPlayerPositionInGame());
	boardG.board.GetBalls();
	boardG.board.GetBoxes();
	boardG.previous = prev;
	boardG.price = price;
	boardG.move = move;
	boardG.pos1 = posFirst;
	boardG.pos2 = posSecond;
	boardG.code = code;
}


