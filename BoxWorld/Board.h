#pragma once
#include <optional>
#include <array>
#include <cstdint>
#include <iostream>
#include "Elements.h"
#include "Ball.h"
#include "Box.h"
#include "Player.h"
#include "Wall.h"
#include "Road.h"
#include "Level.h"
#include <list>

class Board
{
public:
	enum class State
	{
		None,
		Win,
		Lose
	};

public:
	Board() = default;
	Board(const Board& board);
	~Board();
	Board& operator=(const Board& board);

	friend std::ostream& operator <<(std::ostream& out, Board& board);
	void SetLevelFormat(uint16_t levelNr);
	Level GetLevelFormat()const;
	uint16_t GetLevelNumber() const;

	void RenderSprites(sf::RenderWindow* window);

	Elements::Position GetPositionPlayerInGame();

	void DirectBoxAndPlayer(Player&);
	void DirectBoxAndPlayerOnBall(Player&);
	void DirectBoxAndPlayerOnBallBallUnder(Player&);
	void DirectPlayer(Player&);
	void DirectPlayerOnBall(Player&);
	void DirectPlayerOnBallBallUnder(Player&);
	void DirectPlayerBallUnder(Player&);
	void SwapBoardElements(Elements::Position, Elements::Position);
	void SwapElements(std::shared_ptr<Elements>&, std::shared_ptr<Elements>&, Elements::Position, Elements::Position);

	template<typename T>
	bool IsElementCollision(Elements::Position position)
	{
		return std::dynamic_pointer_cast<T>(m_board[position.first][position.second]) != nullptr;
	}

	bool PossibleToMove(Player, Elements::Position directionAdd);
	bool IsBallVisible(Elements::Position);
	bool IsBallInList(Elements::Position);
	void SetVisible(Elements::Position);
	void SetInvisible(Elements::Position);
	void SetVisibility(Elements::Position, bool);
	void InsertBallFromList(Elements::Position);
	uint8_t SearchBallInList(Elements::Position);

	void GetBoxes();
	void GetBalls();
	Elements::Position GetPlayerPositionInGame();
	std::shared_ptr<Elements> GetElement(Elements::Position);
	std::shared_ptr<Elements>& operator[](const Elements::Position&);
	const std::shared_ptr<Elements>& operator[](const Elements::Position&) const;

	Board::State Check();
	bool CheckBoxState(Elements::Position nextPosition);

private:
	const static uint8_t m_generalLine = BOARD_SIDE_SIZE;
	const static uint8_t m_generalColumn = BOARD_SIDE_SIZE;
	Level m_currentLevelFormat;
	Elements::Position m_playerPositionInGame;
	std::vector<std::shared_ptr<Elements>> m_boxesList, m_ballsList;
	std::array< std::array<std::shared_ptr<Elements>, m_generalLine>, m_generalColumn> m_board;
	State m_state;
};
