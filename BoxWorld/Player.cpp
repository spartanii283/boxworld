#include "Player.h"



Player::Player()
{
}

Player::~Player()
{
}

Player::Player(Position position, sf::Texture& textureSheet) : Elements(position)
{
	this->CreateAnimationComponent(textureSheet);

	this->m_animationComponent->AddAnimation("PLAYER_TEST", 100.f, 0, 1, 2, 1, 90);
}

Player::Player(Position position) : Elements(position)
{
	if (!m_playerTexture.loadFromFile("../Resources/PlayerLeftFront.png"))
	{
		std::cout << "Error could not load player image" << std::endl;
	}

	this->setTexture(m_playerTexture);
	this->setPosition(position.second * SPRITE_SIZE, position.first * SPRITE_SIZE);
}

Player::Player(const Player& player2)
{
	SetPosition(player2.m_position);
}

Player& Player::operator=(const Player& player2)
{
	this->SetPosition(player2.m_position);
	return *this;
}

void Player::SetDirection(const Position direction)
{
	m_direction = direction;
}

Elements::Position Player::GetDirection() const
{
	return m_direction;
}

std::ostream& operator<<(std::ostream& out, const Player& player)
{
	out << "@";
	return out;
}
