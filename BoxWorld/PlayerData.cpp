#include "PlayerData.h"

PlayerData::PlayerData()
{
	std::fstream f("Best score.txt");
	for (auto i = 0; i < NUMBER_OF_LEVELS; i++)
	{
		f >> m_bestScore[i].first;
		f >> m_bestScore[i].second;
		m_playerMoves = 0;
		m_boxMoves = 0;
	}
	f.close();
}

PlayerData::PlayerData(
	std::array <BestScore, NUMBER_OF_LEVELS> bestScore,
	uint16_t lastLevel
) : m_bestScore(bestScore), m_lastLevel(lastLevel)
{
}

PlayerData::BestScore PlayerData::GetBestScoreOnLevel(uint16_t level) const
{
	return m_bestScore[level - 1];
}

void PlayerData::SetBestScore(PlayerData::BestScore bestScore, uint16_t level)
{
	std::fstream f("Best score.txt", std::ios::out | std::ios::trunc);
	m_bestScore[level - 1] = bestScore;
	for (auto i : m_bestScore)
	{
		f << std::to_string(i.first) << " " << std::to_string(i.second) << '\n';
	}
	f.close();
}

bool PlayerData::IsBestScore(uint16_t level)
{
	if (m_playerMoves < m_bestScore[level - 1].first || m_bestScore[level - 1].first == 0)
	{
		SetBestScore(PlayerData::BestScore(m_playerMoves, m_boxMoves), level);
		return true;
	}
	return false;
}

uint16_t PlayerData::GetPlayerMoves() const
{
	return m_playerMoves;
}

void PlayerData::SetPlayerMoves(uint16_t playerMoves)
{
	m_playerMoves = playerMoves;
}

uint16_t PlayerData::GetBoxMoves() const
{
	return m_boxMoves;
}

void PlayerData::SetBoxMoves(uint16_t boxMoves)
{
	m_boxMoves = boxMoves;
}

uint16_t PlayerData::GetLastLevel() const
{
	return m_lastLevel;
}

void PlayerData::SetLastLevel(uint16_t lastLevel)
{
	m_lastLevel = lastLevel;
}
