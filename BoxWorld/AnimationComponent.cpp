﻿#include "AnimationComponent.h"

AnimationComponent::AnimationComponent(sf::Sprite sprite, sf::Texture& textureSheet)
	:m_sprite(sprite), m_textureSheet(textureSheet)
{
}

AnimationComponent::~AnimationComponent()
{
	for (auto& i : this->m_animations)
	{
		delete i.second;
	}
}

//Functions

void AnimationComponent::AddAnimation(
	const std::string key,
	float animationSpeed,
	int startFrameX, int startFrameY, int endFramesX, int endFramesY, int edge)
{
	this->m_animations[key] = new Animation(
		this->m_sprite, this->m_textureSheet, 
		animationSpeed,
		startFrameX, startFrameY, endFramesX, endFramesY, edge);
}

void AnimationComponent::Play(const std::string key, const float& dt)
{
	this->m_animations[key]->Play(dt);
}
