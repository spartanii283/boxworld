#pragma once
#include "Elements.h"
#include "Constants.h"

class Wall : public Elements
{
public:
	Wall (Position position);
	Wall();
	~Wall();

	friend std::ostream& operator<<(std::ostream& out, const Wall& ball);

private:
	sf::Texture m_wallTexture;
};
