﻿#pragma once

#include <iostream>
#include <string>
#include <map>

#include "SFML/System.hpp"
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "SFML/Network.hpp"

class AnimationComponent
{
private:
	class Animation
	{
	public:
		sf::Sprite m_sprite;
		sf::Texture& m_textureSheet;
		float m_animationSpeed;
		float m_timer;
		int m_edge;
		sf::IntRect m_startRectangular;
		sf::IntRect m_currentRectangular;
		sf::IntRect m_endRectangular;

		Animation(sf::Sprite sprite, sf::Texture& textureSheet,
			float animationSpeed,
			int startFrameX, int startFrameY, int endFramesX, int endFramesY, int edge)
			:m_sprite(sprite), m_textureSheet(textureSheet), m_animationSpeed(animationSpeed), m_edge(edge)
		{
			this->m_timer = 0.f;

			this->m_startRectangular = sf::IntRect(startFrameX, startFrameY, edge, edge);
			this->m_currentRectangular = this->m_startRectangular;
			this->m_endRectangular = sf::IntRect(endFramesX, endFramesY, edge, edge);

			this->m_sprite.setTexture(this->m_textureSheet, true);
			this->m_sprite.setTextureRect(this->m_startRectangular);
		}

		//Functions
		void Play(const float& dt)
		{
			//Update timer
			this->m_timer += 10.f * dt;
			if (this->m_timer >= this->m_animationSpeed)
			{
				//Reset timer
				this->m_timer = 0.f;

				//Animates
				if (this->m_currentRectangular != this->m_endRectangular)
				{
					this->m_currentRectangular.left += this->m_edge;
				}
				else //Reset
				{
					this->m_currentRectangular.left = this->m_startRectangular.left;
				}

				this->m_sprite.setTextureRect(this->m_currentRectangular);
			}
		}

		void Reset()
		{
			this->m_timer = 0.f;
			this->m_currentRectangular = this->m_startRectangular;
		}
	};

	sf::Sprite m_sprite;
	sf::Texture m_textureSheet;
	std::map<std::string, Animation*> m_animations;

public:
	AnimationComponent(sf::Sprite sprite, sf::Texture& textureSheet);
	virtual ~AnimationComponent();

	//Functions
	void AddAnimation(const std::string key,
		float animationSpeed,
		int startFrameX, int startFrameY, int endFramesX, int endFramesY, int edge);

	void Play(const std::string key, const float& dt);
};
