#pragma once
#include "Player.h"
#include "Box.h"
#include "Ball.h"
#include "Elements.h"
class Level
{

public:
	uint16_t m_levelNumber;

public:
	Level();

	std::vector<std::string> ScanLevelFromFile();
	void SetFileName(std::string fileName);
	void SetInitialPositions(std::vector<std::string> levelType);
	void SetFirstPositionPlayer(Elements::Position positionPlayer);
	void SwapPlayerBoxPositions(Elements::Position positionPlayer, Elements::Position positionBox);
	void SetLevelNumber(uint16_t& levelNr);

	Elements::Position GetInitialPostionPlayer() const;
	std::vector<Elements::Position> GetInitialPositionBoxes() const;
	std::vector<Elements::Position> GetInitialPositionBalls() const;
	std::vector<Elements::Position> GetInitialPositionWalls() const;
	std::vector<Elements::Position> GetInitialPositionPaths() const;
	uint16_t GetNumberOfBoxesAndBalls() const;
	std::string GetFileName();

private:
	uint16_t m_numberOfBoxesAndBalls;
	std::vector<Elements::Position> m_boxes;
	std::vector<Elements::Position> m_balls;
	std::vector<Elements::Position> m_walls;
	std::vector<Elements::Position> m_paths;
	Elements::Position m_playerInLevel;
	std::string m_fileName;
};
