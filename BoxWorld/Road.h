#pragma once
#include "Elements.h"
#include "Constants.h"

class Road: public Elements
{
public:
	Road();
	Road(Position position);
	friend std::ostream& operator<<(std::ostream& out, const Road& road);
	~Road();

private:
	sf::Texture m_roadTexture;
};

