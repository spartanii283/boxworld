#pragma once
#include "Elements.h"
#include "Constants.h"

class Ball: public Elements
{
public:
	Ball();
	Ball(Position position);
	Ball(const Ball& ball2);
	Ball& operator =(const Ball& ball2);

	void SetBallShow(const bool show);
	bool GetBallShow() const;

	friend std::ostream& operator<<(std::ostream& out, const Ball&  ball);
	~Ball();
private:
	sf::Texture m_ballTexture;
	bool m_show;
};