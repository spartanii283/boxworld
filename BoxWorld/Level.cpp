#include "Level.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

Level::Level()
{
}

std::vector<std::string> Level::ScanLevelFromFile()
{
	std::ifstream levelFile;
	levelFile.open(m_fileName);

	std::vector<std::string> m_levelType;
	std::string lineToRead;
	std::string number;
	std::getline(levelFile, number);
	m_levelNumber = std::stoi(number);
	for (auto i = 0; i < 16; i++)
	{
		std::getline(levelFile, lineToRead);
		m_levelType.push_back(lineToRead);
	}
	levelFile.close();
	return m_levelType;
}

void Level::SetFileName(std::string fileName)
{
	m_fileName = fileName;
}

void Level::SetInitialPositions(std::vector<std::string> levelType)
{
	uint8_t lineIndex = 0;
	uint16_t numberBoxesBalls = 0;

	for (auto matrixLine : levelType)
	{
		std::istringstream lineToReadStream(matrixLine);
		char charPassingLine;
		uint8_t columnIndex = 0;
		Elements element;
		for (; lineToReadStream >> charPassingLine; columnIndex++)
		{
			switch (charPassingLine)
			{
			case '#':
				m_walls.push_back(Elements::Position(lineIndex, columnIndex));
				break;

			case '$':
				m_boxes.push_back(Elements::Position(lineIndex, columnIndex));
				numberBoxesBalls++;
				break;

			case '.':
				m_balls.push_back(Elements::Position(lineIndex, columnIndex));
				numberBoxesBalls++;
				break;

			case '@':
				SetFirstPositionPlayer(Elements::Position(lineIndex, columnIndex));
				break;

			case 'S':
				m_paths.push_back(Elements::Position(lineIndex, columnIndex));
				break;

			case 'B':
				m_paths.push_back(Elements::Position(lineIndex, columnIndex));
				break;

			default:
				std::cout << "The file contains unexpected symbols.";
			}
		}
		lineIndex++;
	}
	m_numberOfBoxesAndBalls = numberBoxesBalls;
}

void Level::SetFirstPositionPlayer(Elements::Position position)
{
	m_playerInLevel = position;
}

void Level::SwapPlayerBoxPositions(Elements::Position positionPlayer, Elements::Position positionBox)
{
	std::vector<Elements::Position>::iterator iteratorPositions;
	for (iteratorPositions = m_boxes.begin(); iteratorPositions != m_boxes.end(); ++iteratorPositions)
	{
		if (*iteratorPositions == positionBox)
		{
			*iteratorPositions = positionPlayer;
		}
	}
}

void Level::SetLevelNumber(uint16_t& levelNr)
{
	m_levelNumber = levelNr;
}

Elements::Position Level::GetInitialPostionPlayer() const
{
	return m_playerInLevel;
}

std::vector<Elements::Position> Level::GetInitialPositionBoxes() const
{
	return m_boxes;
}

std::vector<Elements::Position> Level::GetInitialPositionBalls() const
{
	return m_balls;
}

std::vector<Elements::Position> Level::GetInitialPositionWalls() const
{
	return m_walls;
}

std::vector<Elements::Position> Level::GetInitialPositionPaths() const
{
	return m_paths;
}

uint16_t Level::GetNumberOfBoxesAndBalls() const
{
	return m_numberOfBoxesAndBalls;
}

std::string Level::GetFileName()
{
	return m_fileName;
}
