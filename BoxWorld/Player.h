#pragma once
#include <cstdint>
#include <array>
#include "Elements.h"
#include "Constants.h"
#include <SFML/Graphics.hpp>

class Player : public Elements
{
public:
	Player();
	virtual ~Player();
	Player(Position position, sf::Texture& textureSheet);
	Player(Position position);
	Player(const Player& player2);
	Player& operator =(const Player& player2);

	void SetDirection(const Position direction);
	Position GetDirection() const;

	friend std::ostream& operator <<(std::ostream& out, const Player& player);

private:
	sf::Texture m_playerTexture;
	Position m_direction = DIR_UP;
};

