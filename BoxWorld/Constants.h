#pragma once
#include "Elements.h"

#define TILE_SIZE 35.f
#define NUMBER_OF_LEVELS 15
#define SPRITE_SIZE 43
#define BOARD_SIDE_SIZE 16

constexpr auto DIR_UP = Elements::Position(-1, 0);
constexpr auto DIR_DOWN = Elements::Position(1, 0);
constexpr auto DIR_LEFT = Elements::Position(0, -1);
constexpr auto DIR_RIGHT = Elements::Position(0, 1);
