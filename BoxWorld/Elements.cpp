#include "Elements.h"

void Elements::SetTexture(sf::Texture& texture)
{
	this->m_sprite.setTexture(texture);
}

void Elements::CreateAnimationComponent(sf::Texture textureSheet)
{
	this->m_animationComponent = new AnimationComponent(this->m_sprite, textureSheet);
}

void Elements::InitTextures()
{
	this->m_textures["PLAYER_TEST"].loadFromFile(".. / Resources / BoxworldOriginal3");
}

std::map<std::string, sf::Texture> Elements::GetTexture()
{
	return m_textures;
}

Elements::Elements(Position position) : m_position(position)
{
}

Elements::Elements()
{
}

Elements::Position Elements::GetPosition() const
{
	return m_position;
}

void Elements::SetPosition(Position position)
{
	m_position = position;
}

Elements::~Elements()
{
}

std::ostream& operator<<(std::ostream& out, const Elements& elem)
{
	out << "DA";
	return out;
}
