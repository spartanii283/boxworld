#pragma once
#include <utility>
#include <cstdint>

#include "AnimationComponent.h"

#include "SFML/System.hpp"
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"

class Elements : public sf::Sprite
{
public:
	using Position = std::pair<uint16_t, uint16_t>;
	
public:
	void SetTexture(sf::Texture& texture);
	void CreateAnimationComponent(sf::Texture textureSheet);
	void InitTextures();
	std::map<std::string, sf::Texture> GetTexture();

	Elements(Position position);
	Elements();
	Position GetPosition() const;
	void SetPosition(Position position);
	virtual ~Elements();

	friend std::ostream& operator<<(std::ostream& out, const Elements&  elem);
	
protected:
	Position m_position;
	
	sf::Sprite m_sprite;
	AnimationComponent* m_animationComponent;
	std::map<std::string, sf::Texture> m_textures;
};

